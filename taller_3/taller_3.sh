#!/usr/bin/env bash

###############################################################################
##########                                                           ##########
##########                 Desarrollo taller 3                       ##########
##########                                                           ##########
##########  Autor: German Melo                                       ##########
##########  Fecha: dd/mm/aaaa                                        ##########
##########                                                           ##########
###############################################################################

######## Importación del vectorial Municipios al location municipios.  ########

# Comando grass genera un nuevo location con EPSG:3116 y en el ubica la
# información del vectorial Municipios.
echo "Iniciando GRASS..."
echo "Clean old datasets"
rm -rf $HOME/grassdata/{municipios,geography,precipitation,temperature}
grass74 -e -c epsg:3116 $HOME/grassdata/municipios
g.extension extension=r.clip
echo "Change Location and Mapset"
g.mapset -c location=municipios mapset=PERMANENT
history -w; history -r $HOME/municipios/PERMANENT/.bash_history; HISTFILE=$HOME/municipios/PERMANENT/.bash_history
echo "Import data of municipios"
v.import -o input=./Mapas_colombia/Municipios.shp output=municipios
echo "Select data from Municipios with value 'CAP.DEPTO' in attribute 'ENTIDAD_TE'"
v.extract --overwrite input=municipios@PERMANENT where="ENTIDAD_TE LIKE 'CAP. DEPTO%'" output=capitales
echo "Show Capitales table data"
g.gui.dbmgr map=capitales@PERMANENT
echo "Make new mapset with geographic coordinates"
grass74 -e -c epsg:4326 $HOME/grassdata/geography
echo "Import MDE of Colombia"
# Creating new mapset
grass74 -e -c ./Mapas_colombia/Colombia_MDE.tif $HOME/grassdata/geography
# Change mapset
g.mapset -c location=geography mapset=PERMANENT
# import MDE raster file
r.in.gdal --overwrite -o input=./Mapas_colombia/Colombia_MDE.tif output=Colombia_MDE
# import Colombia border vector file
v.in.ogr --overwrite -o input=./Mapas_colombia/Colombia.shp output=Colombia
echo "Import import anual average long term precipitation data"
grass74 -e -c epsg:4326 $HOME/grassdata/precipitation
# Change mapset
g.mapset -c location=precipitation mapset=PERMANENT
# import precipitation raster file
r.in.gdal --overwrite -o input=NETCDF:"Precip_Temp/precip.mon.ltm.v401.nc":precip output=precipitation
# Make average precipitation file
v.proj --overwrite location=geography mapset=PERMANENT input=Colombia output=Colombia
# Estableciendo Mascara para el corte
g.region -p vector=Colombia align=precipitation.1
r.mask vector=Colombia@PERMANENT
r.series --overwrite input="`g.list pattern='precipitation.*' sep=, type=raster`" output=precipitation.mean method=average
# Import Colombia border
v.proj --overwrite location=geography mapset=PERMANENT input=Colombia output=Colombia
# Estableciendo Mascara para el corte
g.region raster=Colombia@PERMANENT
r.mask vector=Colombia@PERMANENT
# Estableciendo Precipitacion promedio solo para colombia
r.mapcalc --overwrite expression="precipitation_mean_colombia=precipitation.mean@PERMANENT"
# Elimina la mascara
r.mask -r
echo "import anual average long term temperature data"
grass74 -e -c epsg:4326 $HOME/grassdata/temperature
# Change mapset
g.mapset -c location=temperature mapset=PERMANENT
# import temperature raster file
r.in.gdal --overwrite -o input=NETCDF:"Precip_Temp/air.mon.ltm.v401.nc":air output=temperature
# Make average temperature file
# Import Colombia border
v.proj --overwrite location=geography mapset=PERMANENT input=Colombia output=Colombia
# Estableciendo Mascara para el corte
g.region -p vector=Colombia align=temperature.1
r.mask vector=Colombia@PERMANENT
r.series --overwrite input="`g.list pattern='temperature.*' sep=, type=raster`" output=temperature.mean method=average
# Estableciendo Precipitacion promedio solo para colombia
r.mapcalc --overwrite expression="temperature_mean_colombia=temperature.mean@PERMANENT"
# Elimina la mascara
r.mask -r
echo "Import MDE to Municipios location"
# Importing data of vectorial maps to MAGNA-SIRGAS location
echo "Change Location and Mapset"
grass74 -e -c epsg:3116 $HOME/grassdata/municipios
g.mapset -c location=municipios mapset=PERMANENT
g.region -p
# Import data of Colombia
v.proj --overwrite location=geography mapset=PERMANENT input=Colombia output=Colombia
g.region -p vector=Colombia
r.proj --overwrite location=geography mapset=PERMANENT input=Colombia_MDE output=Colombia_MDE memory=3000 resolution=900
echo "Import precipitation anual data to Municipios location"
# Import data of precipitation
g.region -p vector=Colombia align=Colombia_MDE
r.mask vector=Colombia@PERMANENT
r.proj -n --overwrite location=precipitation mapset=PERMANENT input=precipitation_mean_colombia output=precipitation_mean_colombia
echo "Import temperature anual data to Municipios location"
# Import data of temperature
r.proj -n --overwrite location=temperature mapset=PERMANENT input=temperature_mean_colombia output=temperature_mean_colombia
r.mask -r
echo "Dibujar perfil compuesto."
r.profile -g input=Colombia_MDE@PERMANENT coordinates=543223.770301,680190.432905,1600513.60819,1082860.39244 > ./perfiles_generados/mde.txt
r.profile -g input=precipitation_mean_colombia@PERMANENT coordinates=543223.770301,680190.432905,1600513.60819,1082860.39244 > ./perfiles_generados/precipitation.txt
r.profile -g input=temperature_mean_colombia@PERMANENT coordinates=543223.770301,680190.432905,1600513.60819,1082860.39244 > ./perfiles_generados/temperature.txt
echo "Clasificar Mapa de topografias."
r.reclass --overwrite input=Colombia_MDE@PERMANENT output=Colombia_MDE_3_classes rules=/home/fercho/Documents/German_USTA/taller_3/clases_altura/clases_altura.recl.rules
r.reclass --overwrite input=Colombia_MDE@PERMANENT output=Colombia_MDE_0_500 rules=./clases_altura/0_500.recl.rules
r.reclass --overwrite input=Colombia_MDE@PERMANENT output=Colombia_MDE_501_1500 rules=./clases_altura/501_1500.recl.rules
r.reclass --overwrite input=Colombia_MDE@PERMANENT output=Colombia_MDE_1500_rest rules=./clases_altura/1500_*.recl.rules
echo "Univariate statistics for precipitation"
echo "0-500m"
r.mask raster=Colombia_MDE_0_500@PERMANENT
r.univar -e map=precipitation_mean_colombia@PERMANENT
r.univar -e map=precipitation_mean_colombia@PERMANENT > ./reportes/precipitation_MDE_0_500.txt
r.report map=precipitation_mean_colombia@PERMANENT units=h,c,p >> ./reportes/precipitation_MDE_0_500.txt
r.mask -r
echo "501-1500m"
r.mask raster=Colombia_MDE_501_1500@PERMANENT
r.univar -e map=precipitation_mean_colombia@PERMANENT
r.univar -e map=precipitation_mean_colombia@PERMANENT > ./reportes/precipitation_MDE_501_1500.txt
r.report map=precipitation_mean_colombia@PERMANENT units=h,c,p >> ./reportes/precipitation_MDE_501_1500.txt
r.mask -r
echo ">1500m"
r.mask raster=Colombia_MDE_1500_rest@PERMANENT
r.univar -e map=precipitation_mean_colombia@PERMANENT
r.univar -e map=precipitation_mean_colombia@PERMANENT > ./reportes/precipitation_1500_rest.txt
r.report map=precipitation_mean_colombia@PERMANENT units=h,c,p >> ./reportes/precipitation_1500_rest.txt
r.mask -r
echo "Univariate statistics for temperature"
echo "0-500m"
r.mask raster=Colombia_MDE_0_500@PERMANENT
r.univar -e map=temperature_mean_colombia@PERMANENT
r.univar -e map=temperature_mean_colombia@PERMANENT > ./reportes/temperature_MDE_0_500.txt
r.report map=temperature_mean_colombia@PERMANENT units=h,c,p >> ./reportes/temperature_MDE_0_500.txt
r.mask -r
echo "501-1500m"
r.mask raster=Colombia_MDE_501_1500@PERMANENT
r.univar -e map=temperature_mean_colombia@PERMANENT
r.univar -e map=temperature_mean_colombia@PERMANENT > ./reportes/temperature_MDE_501_1500.txt
r.report -e map=temperature_mean_colombia@PERMANENT units=h,c,p >> ./reportes/temperature_MDE_501_1500.txt
r.mask -r
echo ">1500m"
r.mask raster=Colombia_MDE_1500_rest@PERMANENT
r.univar -e map=temperature_mean_colombia@PERMANENT
r.univar -e map=temperature_mean_colombia@PERMANENT > ./reportes/temperature_1500_rest.txt
r.report map=temperature_mean_colombia@PERMANENT units=h,c,p >> ./reportes/temperature_1500_rest.txt
r.mask -r
echo "####################################################################"
echo "#################  Finish  #########################################"
echo "#################  Thanks for use this software  ###################"
echo "####################################################################"
