set GISBASE=C:\Program Files\GRASS GIS 7.4.1
call "%GISBASE%\etc\env.bat"
set PATH=%PATH%;%GISBASE%
rem g.gui wxpython para iniciar gui
REM cd C:/GRASS_GIS/talleres/taller_4 (considere la ruta donde quiere llevar a cabo todos los procedimientos)
REM si el ste comando no crea la locacion, creela en el entorno visual
grass74 -e -c epsg:4326 C:/grassdata/preciptacion
REM pensando en crear mapasets y no funciono g.mapset --overwrite -c mapset=precip location=preciptacion_temperatura
r.in.gdal --overwrite -o input=NETCDF:"C:/GRASS_GIS/talleres/taller_4/Precip_Temp/precip.mon.total.v401.nc":precip output=precipitationmeses
REM el ste comando cambia a la region 
g.region raster=precipitationmeses.1@PERMANENT 
REM crea una bd temporal strds
t.create --overwrite type=strds  output=precipita_monthly_00_14 title="precipita_monthly_00_14" description="Precipitacion entre 1900_2014"
g.list --overwrite type=raster pattern="precipitationmeses*" output=maplist_precip.txt 
t.register -i input=precipita_monthly_00_14 file=maplist_precip.txt start="1900-01-01" increment="1 months"
t.info input=precipita_monthly_00_14
REM agregar datos de meses a anos
t.rast.aggregate --overwrite input=precipita_monthly_00_14@PERMANENT output=precipita_year_00_14 basename="precipita_year" granularity="1 year" method=sum
g.list --overwrite type=raster pattern="precipita_year*" output=maplist_precip_year.txt
t.register -i input=precipita_year_00_14 file=maplist_precip_year.txt start="1900-01-01" increment="1 year"
t.info input=precipita_year_00_14
t.list type=strds


REM pensando en crear un vector de tipo vectorial temporal 
v.in.ascii --overwrite -t input=punto_vect.csv output=ascii_precip separator="comma"
v.db.addtable --overwrite map=ascii_precip columns="slope double precision, namepunto varchar(15)" 
t.rast.what --overwrite points=ascii_precip@PERMANENT strds=precipita_year_00_14@PERMANENT output=salida_precip.txt


t.rast.extract --overwrite input=precipita_year_00_14 output=anual_precip_41_70 where="start_time BETWEEN '1941-01-01' AND '1971-01-01'" 
t.rast.list --overwrite input=anual_precip_41_70 columns=name output=map_list_precip_year4170.txt
t.register --overwrite -i input=anual_precip_41_70@PERMANENT file=map_list_precip_year4170.txt start="1941-01-01" increment="1 year"
t.info input=anual_precip_41_70
t.rast.univar --overwrite input=anual_precip_41_70@PERMANENT
t.rast.aggregate --overwrite input=anual_precip_41_70@PERMANENT output=precipita_average basename="precipita_average" granularity="30 year" method=average
g.list --overwrite type=raster pattern="precipita_average*" output=map_list_precip_average_41_70.txt
t.register --overwrite -i input=precipita_average file=map_list_precip_average_41_70.txt start="1941-01-01" increment="30 year"
t.info input=precipita_average 


t.rast.extract --overwrite input=precipita_year_00_14 output=anual_precip_81_010 where="start_time BETWEEN '1981-01-01' AND '2011-01-01'" 
g.list --overwrite type=raster pattern="anual_precip_81_010*" output=map_list_precip_year8110.txt
t.register -i input=anual_precip_81_010 file=map_list_precip_year8110.txt start="1981-01-01" increment="1 year"
t.info input=anual_precip_81_010
t.rast.univar --overwrite input=anual_precip_81_010@PERMANENT
t.rast.aggregate --overwrite input=anual_precip_81_010@PERMANENT output=precipita_2average basename="precipita_2average" granularity="30 year" method=average
g.list --overwrite type=raster pattern="precipita_2average*" output=map_list_precip_average_81_10.txt
t.register --overwrite -i input=precipita_2average file=map_list_precip_average_81_10.txt start="1981-01-01" increment="30 year"
t.info input=precipita_2average 




REM HOJA 2 el proceso esta relacionado con el calculo de la media de los datos entre periodos definidos
r.mapcalc expression=precipitation_diference_porcent = ( ( precipita_2average_1981@PERMANENT - precipita_average_1941@PERMANENT ) / precipita_average_1941@PERMANENT ) *100
 
v.import --overwrite input="C:\GRASS_GIS\talleres\taller_4\shape_region\subregion_ven.shp" output=ven
g.region -p vector=ven align=precipitation_diference_porcent
r.mask --overwrite vector=ven
r.out.ascii --overwrite input=precipitation_diference_porcent output=precip_change
 
 
 
 
 
REM inicia el proceso con temperatura
grass74 -e -c epsg:4326 C:/grassdata/temperatura
Rem no olvidar cambiar localizacion aca
rem gdalinfo input=C:/GRASS_GIS/talleres/taller_4/Precip_Temp/air.mon.mean.v401.nc
r.in.gdal --overwrite -o input=NETCDF:"Precip_Temp/air.mon.mean.v401.nc":air output=temperaturames

REM el ste comando cambia a la region 
g.region raster=temperaturames.1@PERMANENT
REM crea una bd temporal strds
t.create --overwrite type=strds  output=temper_monthly_00_14 title="temper_monthly_00_14" description="Temper entre 1990_2014"
g.list type=raster pattern="temperatura*" output=map_list_temper.txt 
t.register -i input=temper_monthly_00_14 file=map_list_temper.txt start="1900-01-01" increment="1 months"
t.info input=temper_monthly_00_14@PERMANENT

t.rast.aggregate --overwrite input=temper_monthly_00_14@PERMANENT output=temper_year_00_14 basename="temper_year" granularity="1 year" method=average
g.list --overwrite type=raster pattern="temper_year*" output=maplist_temper_year.txt
t.register -i input=temper_year_00_14 file=maplist_temper_year.txt start="1900-01-01" increment="1 year"
t.info input=temper_year_00_14@PERMANENT
t.list type=strds

v.in.ascii --overwrite -t input=punto_vect.csv output=ascii_temper separator="comma"
v.db.addtable --overwrite map=ascii_temper columns="slope double precision, namepunto varchar(15)" 
t.rast.what --overwrite points=ascii_temper@PERMANENT strds=temper_year_00_14@PERMANENT output=salida_temperatur.txt

REM extraer datos entre fechas
rem 41-70

t.rast.extract --overwrite input=temper_year_00_14 output=anual_temper_41_70 where="start_time BETWEEN '1941-01-01' AND '1971-01-01'" 
g.list --overwrite type=raster pattern="anual_temper_41_70*" output=map_list_temper_41_70.txt
REM t.rast.list --overwrite input=anual_temper_41_70 columns=name output=map_list_temper_year4170.txt
t.register --overwrite -i input=anual_temper_41_70@PERMANENT file=map_list_temper_41_70.txt start="1941-01-01" increment="1 year"
t.info input=anual_temper_41_70
rem estadisticas univariadas de los 30 periodos
t.rast.univar --overwrite input=anual_temper_41_70@PERMANENT
rem agregado de los datos en metodo average para temp 41 70
t.rast.aggregate --overwrite input=anual_temper_41_70@PERMANENT output=temperat_average_41_70 basename="temperat_average" granularity="30 year" method=average
g.list --overwrite type=raster pattern="temperat_average*" output=map_list_temper_average_41_70_a.txt
t.register --overwrite -i input=temperat_average_41_70 file=map_list_temper_average_41_70_a.txt start="1941-01-01" increment="30 year"
t.info input=temperat_average_41_70 
t.rast.univar --overwrite input=temperat_average_41_70
rem 81 2010
t.rast.extract --overwrite input=temper_year_00_14 output=anual_temper_81_10 where="start_time BETWEEN '1981-01-01' AND '2011-01-01'" 
g.list --overwrite type=raster pattern="anual_temper_81_10*" output=map_list_temper_81_10.txt
REM t.rast.list --overwrite input=anual_temper_41_70 columns=name output=map_list_temper_year4170.txt
t.register --overwrite -i input=anual_temper_81_10@PERMANENT file=map_list_temper_81_10.txt start="1941-01-01" increment="1 year"
t.info input=anual_temper_81_10
rem estadisticas univariadas de los 30 periodos
t.rast.univar --overwrite input=anual_temper_81_10@PERMANENT
rem agregado de los datos en metodo average para temp 41 70
t.rast.aggregate --overwrite input=anual_temper_81_10@PERMANENT output=temperat_average_81_10 basename="temperat_average" granularity="30 year" method=average
g.list --overwrite type=raster pattern="temperat_average*" output=map_list_temper_average_81_10_a.txt
t.register --overwrite -i input=temperat_average_81_10 file=map_list_temper_average_81_10_a.txt start="1981-01-01" increment="30 year"
t.info input=temperat_average_81_10 
t.rast.univar --overwrite input=temperat_average_81_10
r.mapcalc expression="temperatura_abs = ( temperat_average_1981@PERMANENT - temperat_average_1941@PERMANENT )" 


v.import --overwrite input="Mapas_colombia/Colombia.shp" output=ven
g.region -p vector=ven align=temperatura_abs
REM Creates a MASK for limiting raster operation. 
r.mask --overwrite vector=ven
r.out.ascii --overwrite input=temperatura_abs output=temp_change


